package com.gramaniladari.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gramaniladari.app.models.GNService;
import com.gramaniladari.app.models.TimberTransport;
import com.gramaniladari.app.repositories.GNServiceRepository;
import com.gramaniladari.app.repositories.TimberTransportRepository;

@SpringBootApplication
public class GramaNiladariApplication implements CommandLineRunner{

	 @Autowired
    private GNServiceRepository gnServiceRepository;

    @Autowired
    private TimberTransportRepository timberTransportRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(GramaNiladariApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		 // Clean up database tables
//		gnServiceRepository.deleteAllInBatch();
//		timberTransportRepository.deleteAllInBatch();
//		
//		GNService gnService = new GNService("TIMBER",12L,new TimberTransport("KALPANI", "NEGOMBO"));
//		
//		gnServiceRepository.save(gnService);
		
		
	}
	
	

}
