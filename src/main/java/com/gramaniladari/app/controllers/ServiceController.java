package com.gramaniladari.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/services")
public class ServiceController {
	
	@GetMapping({"","/"})
	public String index(Model model) {
        return "index";
    }
	
	@GetMapping("/business")
	public String business(Model model) {
        return "/pages/forms/business";
    }
	
	@GetMapping("/timber")
	public String timber(Model model) {
        return "/pages/forms/timber";
    }
	
	
	@GetMapping("/treecutting")
	public String treecutting(Model model) {
        return "/pages/forms/treecutting";
	
	}
	
	@GetMapping("/businessfirm")
	public String businessfirm(Model model) {
        return "/pages/forms/businessfirm";
    }
	
	@GetMapping("/gunlisence")
	public String gunlisence(Model model) {
        return "/pages/forms/gunlisence";
    }
	

}
