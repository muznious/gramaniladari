package com.gramaniladari.app.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.*;

@Entity
@Table(name = "services")
public class GNService implements Serializable {

	
 	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "service_id")
    private Long id;
 	
 	@Column(name = "service_category")
 	private String category;
 	
 	@Column(name = "individual_id")
 	private Long individualId;
 	
 	
 	@OneToOne(cascade = CascadeType.ALL, mappedBy="gnService")
 	private TimberTransport timberTransport;


	public GNService(String category, Long individualId, TimberTransport timberTransport) {
		super();
		this.category = category;
		this.individualId = individualId;
		this.timberTransport.setGnService(this);
	}
 	
	 
}
