package com.gramaniladari.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gramaniladari.app.models.GNService;
import com.gramaniladari.app.models.TimberTransport;

public interface GNServiceRepository extends JpaRepository<GNService, Long> {
	

}
