package com.gramaniladari.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gramaniladari.app.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}