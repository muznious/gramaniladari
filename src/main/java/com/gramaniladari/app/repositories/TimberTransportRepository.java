package com.gramaniladari.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gramaniladari.app.models.TimberTransport;

public interface TimberTransportRepository extends JpaRepository<TimberTransport, Long> {
	

}
