package com.gramaniladari.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.gramaniladari.app.models.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByNic(String nic);
}